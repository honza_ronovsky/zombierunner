﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Daycycle : MonoBehaviour {
    [SerializeField]
    float timeScale = 60f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float newXRot =Time.deltaTime / timeScale*360;
        /* if (newXRot >= 180) {
             newXRot = -180;
         }*/
        //Debug.Log(newXRot);
        transform.RotateAround(transform.position, Vector3.forward, newXRot);

    }
}
