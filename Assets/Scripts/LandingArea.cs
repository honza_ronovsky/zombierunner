﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingArea : MonoBehaviour {

    [SerializeField][Range(0, 5f)]
    float timeToCheck = 1f;

    //cache
    Collider colliderCheck;

    //state
    float lastCollisionTime;

    void Awake() {
        colliderCheck = GetComponent<Collider>();
    }

    // Use this for initialization
    void Start() {
        lastCollisionTime = Time.time;
    }

    // Update is called once per frame
    void Update() {
        if (Time.time - timeToCheck >= lastCollisionTime) {
            Debug.Log("Clear for landing");
        }
	}

    void OnTriggerStay(Collider coll) {
        Debug.Log("Trigger stays");
        lastCollisionTime = Time.time;
    }
}
