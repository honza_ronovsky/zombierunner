﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helicopter : MonoBehaviour {

    [SerializeField]
    AudioClip helicopterFlying;
    [SerializeField]
    AudioClip helicopterOnTheWay;

    bool isHelicopterCalled = false;

    AudioSource audioSource;

    // Use this for initialization
    void Awake () {
        audioSource = GetComponent<AudioSource>();
	}

    void Start() {
    }
	
	// Update is called once per frame
	void Update () {
        CallHelicopter();
    }

    void CallHelicopter() {
        if (Input.GetButton("CallHeli")) {
            if (!isHelicopterCalled) {
                Debug.Log("Calling helicopter");
                StartCoroutine(StartHelicopter());
                isHelicopterCalled = true;
            }
        }
    }

    IEnumerator StartHelicopter() {
        audioSource.clip = helicopterOnTheWay;
        audioSource.spatialBlend = 0f;
        audioSource.Play(); // helicopter responds to player
        yield return new WaitForSeconds(audioSource.clip.length);
        audioSource.loop = true;
        audioSource.clip = helicopterFlying;
        audioSource.spatialBlend = 1f;
        audioSource.Play();

    }
}
