﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Camera))]
public class Eyes : MonoBehaviour {
    float zoomViewportModifer = 2f;

    //cache
    Camera eyes;
    float origViewport;

    //state
    bool isZoomed = false;

	
	void Awake () {
        eyes = GetComponent<Camera>();
	}

    void Start() {
        origViewport = eyes.fieldOfView;
    }
	
	// Update is called once per frame
	void Update () {
        ZoomViewport();
	}

    void ZoomViewport() {
        if (Input.GetButton("Zoom")) {
            if(!isZoomed) {
                Debug.Log("Zooming");
                eyes.fieldOfView /= zoomViewportModifer;
                isZoomed = true;
            }
        } else {
            eyes.fieldOfView = origViewport;
            isZoomed = false;
        }
    }
}
