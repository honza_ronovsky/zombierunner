﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField]
    bool isRespawning = true;
    [SerializeField]
    Transform spawningPointsParent;

    Transform[] spawningPoints;

	// Use this for initialization
	void Start () {
        spawningPoints = spawningPointsParent.GetComponentsInChildren<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if(isRespawning) {
            Respawn();
        }

    }

    void Respawn() {
        int randomTransformNumber = UnityEngine.Random.Range(1, spawningPoints.Length); // 1, because first thing in array filled by GetComponentsInChildren contains the parent. Dont ask why.
        Debug.Log(randomTransformNumber);
        transform.position = spawningPoints[randomTransformNumber].transform.position;
        isRespawning = false;
    }
}
